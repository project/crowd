<?php

declare(strict_types=1);

namespace Drupal\Tests\crowd\Kernel;

use Drupal\crowd\Crowd\CrowdConnectorInterface;
use Drupal\user\UserInterface;

/**
 * Defines a class for testing restricted fields access.
 *
 * @group crowd
 */
final class RestrictedFieldsAccessTest extends CrowdKernelTestBase {

  /**
   * Tests changing profile fields on a restricted domain.
   */
  public function testEditingProfileFieldsWithRestrictedDomain(): void {
    $restricted = sprintf('%s@%s', $this->randomMachineName(), $this->restrictedDomain);
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $pass = $this->randomMachineName(15);
    $this->crowdConnector->register($restricted, $restricted, $pass, $given_name, $surname, $display_name);
    $this->crowdConnector->updateUserStatus($restricted);
    $account = \Drupal::service('externalauth.externalauth')
      ->register($restricted, CrowdConnectorInterface::PROVIDER, [
        'name' => $restricted,
        'mail' => $restricted,
        // Set a random password that doesn't allow local login.
        'pass' => $this->randomMachineName(50),
        'status' => 1,
        'given_name' => $given_name,
        'surname' => $surname,
        'display_name' => $display_name,
      ], []);
    assert($account instanceof UserInterface);
    foreach (['given_name', 'surname', 'display_name'] as $field) {
      $this->assertFalse($account->get($field)->access('edit', $account));
    }
  }

}
