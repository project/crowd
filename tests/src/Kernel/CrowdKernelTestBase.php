<?php

declare(strict_types=1);

namespace Drupal\Tests\crowd\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\crowd\Traits\CrowdTestTrait;

/**
 * Defines a class for base test class for Crowd kernel tests.
 */
abstract class CrowdKernelTestBase extends KernelTestBase {

  use CrowdTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'key',
    'system',
    'user',
    'crowd',
    'crowd_test',
    'externalauth',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['system', 'user', 'crowd']);
    $this->installEntitySchema('user');
    $this->installSchema('externalauth', ['authmap']);
    $this->doSetup();
  }

}
