<?php

declare(strict_types=1);

namespace Drupal\Tests\crowd\Traits;

use Drupal\crowd\Crowd\CrowdConnectorInterface;
use Drupal\crowd\Form\SettingsForm;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;

/**
 * Defines a trait for crowd tests.
 */
trait CrowdTestTrait {

  /**
   * Verified role.
   *
   * @var \Drupal\user\RoleInterface
   */
  protected RoleInterface $verifiedRole;

  /**
   * Restricted domain.
   *
   * @var string
   */
  protected string $restrictedDomain;

  /**
   * Connector.
   *
   * @var \Drupal\crowd\Crowd\CrowdConnectorInterface
   */
  protected CrowdConnectorInterface $crowdConnector;

  /**
   * Perform setup.
   */
  protected function doSetup(): void {
    $this->verifiedRole = Role::create([
      'id' => $this->randomMachineName(),
      'label' => $this->randomMachineName(),
    ]);
    $this->verifiedRole->save();
    $this->restrictedDomain = sprintf('%s.dev.null', $this->randomMachineName());
    $this->config(SettingsForm::CROWD_SETTINGS)
      ->set('username', $this->randomMachineName())
      ->set('server_uri', sprintf('https://%s.dev.null/crowd', mb_strtolower($this->randomMachineName())))
      ->set('email_as_username', TRUE)
      ->set('verified_role', $this->verifiedRole->id())
      ->set('restricted_domains', [$this->restrictedDomain])
      ->save();
    $this->config('user.settings')
      ->set('verify_mail', TRUE)
      ->set('register', UserInterface::REGISTER_VISITORS)
      ->save();
    $this->crowdConnector = \Drupal::service('crowd.connector');
  }

  /**
   * Generates a random email address.
   *
   * @return string
   *   Random email.
   */
  protected function randomEmail(): string {
    return sprintf('%s@%s.dev.null', mb_strtolower($this->randomMachineName()), mb_strtolower($this->randomMachineName()));
  }

}
