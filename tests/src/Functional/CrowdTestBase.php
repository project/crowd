<?php

declare(strict_types=1);

namespace Drupal\Tests\crowd\Functional;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\crowd\Crowd\CrowdConnectorInterface;
use Drupal\crowd\Form\SettingsForm;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\crowd\Traits\CrowdTestTrait;
use Drupal\user\UserInterface;

/**
 * Defines a base test class for crowd tests.
 */
abstract class CrowdTestBase extends BrowserTestBase {

  use AssertMailTrait;
  use CrowdTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'crowd',
    'crowd_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->doSetup();
  }

  /**
   * {@inheritdoc}
   */
  protected function getResetUrl(array $mail): ?string {
    $urls = [];
    preg_match('#.+user/reset/.+#', $mail['body'], $urls);
    return $urls[0] ?? NULL;
  }

  /**
   * Register a user with crowd.
   *
   * @param string $email
   *   Email address.
   * @param string $pass
   *   Password.
   * @param string $given_name
   *   Given name.
   * @param string $surname
   *   Surname.
   * @param string $display_name
   *   Display name.
   * @param string|null $username
   *   Username.
   *
   * @return \Drupal\user\UserInterface
   *   Created user.
   */
  protected function registerCrowdAccount(string $email, string $pass, string $given_name, string $surname, string $display_name, ?string $username = NULL): UserInterface {
    $this->crowdConnector->register($username ?: $email, $email, $pass, $given_name, $surname, $display_name);
    $this->crowdConnector->updateUserStatus($username ?: $email);
    $account = \Drupal::service('externalauth.externalauth')
      ->register($username ?: $email, CrowdConnectorInterface::PROVIDER, [
        'name' => $username ?: $email,
        'mail' => $email,
        // Set a random password that doesn't allow local login.
        'pass' => $this->randomMachineName(50),
        'status' => 1,
        'given_name' => $given_name,
        'surname' => $surname,
        'display_name' => $display_name,
      ], []);
    assert($account instanceof UserInterface);
    if ($verified = \Drupal::configFactory()->getEditable(SettingsForm::CROWD_SETTINGS)->get('verified_role')) {
      $account->addRole($verified);
      $account->save();
    }
    $account->passRaw = $pass;
    return $account;
  }

}
