<?php

declare(strict_types=1);

namespace Drupal\Tests\crowd\Functional;

use Drupal\encrypt\EncryptionProfileInterface;
use Drupal\encrypt\Entity\EncryptionProfile;
use Drupal\key\Entity\Key;
use Drupal\key\KeyInterface;

/**
 * Defines a class for TFA integration.
 *
 * @group crowd
 * @coversDefaultClass \Drupal\crowd\Hooks\Tfa
 */
final class TfaIntegrationTest extends CrowdTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'tfa',
    'encrypt',
    'encrypt_test',
    'key',
    'tfa_test_encryption',
    'tfa_test_plugins',
  ];

  /**
   * A test key.
   */
  protected KeyInterface $testKey;

  /**
   * An encryption profile.
   */
  protected EncryptionProfileInterface $encryptionProfile;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $key = Key::create([
      'id' => 'testing_key_128',
      'label' => 'Testing Key 128 bit',
      'key_type' => 'encryption',
      'key_type_settings' => ['key_size' => '128'],
      'key_provider' => 'config',
      'key_provider_settings' => ['key_value' => 'mustbesixteenbit'],
    ]);
    $key->save();
    $this->testKey = $key;

    $encryption_profile = EncryptionProfile::create([
      'id' => 'test_encryption_profile',
      'label' => 'Test encryption profile',
      'encryption_method' => 'test_encryption_method',
      'encryption_key' => $this->testKey->id(),
    ]);
    $encryption_profile->save();
    $this->encryptionProfile = $encryption_profile;

    \Drupal::configFactory()->getEditable('tfa.settings')
      ->set('enabled', TRUE)
      ->set('required_roles', [])
      ->set('allowed_validation_plugins', ['tfa_test_plugins_validation' => 'tfa_test_plugins_validation'])
      ->set('default_validation_plugin', 'tfa_test_plugins_validation')
      ->set('encryption', 'test_encryption_profile')
      ->save();

    $this->verifiedRole->grantPermission('setup own tfa')->save();
  }

  /**
   * Test TFA setup for user.
   */
  public function testTfaWorkflow(): void {
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $account = $this->registerCrowdAccount($email, $pass, $given_name, $surname, $display_name);

    $assert = $this->assertSession();
    $this->drupalLogin($account);
    $this->drupalGet('user/' . $account->id() . '/security/tfa');
    $assert->statusCodeEquals(200);
    $this->clickLink('Set up test application');

    $this->submitForm(['current_pass' => $account->passRaw], 'Confirm');
    $edit = [
      'expected_field' => 'Expected field content',
    ];
    $this->submitForm($edit, 'Verify and save');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains('TFA setup complete.');
    $assert->pageTextContains('Status: TFA enabled');
    $this->drupalLogout();

    $this->drupalLogin($account);
    $assert->addressMatches('/\/tfa\/' . $account->id() . '/');
  }

}
