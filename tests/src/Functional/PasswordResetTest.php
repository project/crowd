<?php

declare(strict_types=1);

namespace Drupal\Tests\crowd\Functional;

use Drupal\Core\Url;
use Drupal\crowd\Crowd\CrowdConnectorInterface;
use Drupal\user\UserInterface;

/**
 * Tests password reset functionality.
 *
 * @group crowd
 *
 * @covers \Drupal\crowd\Hooks\PasswordReset
 */
final class PasswordResetTest extends CrowdTestBase {

  /**
   * Tests user with crowd and Drupal account can reset password.
   */
  public function testPasswordResetProvisionedUser(): void {
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $account = $this->registerCrowdAccount($email, $pass, $given_name, $surname, $display_name);
    $this->drupalGet(Url::fromRoute('user.pass'));
    $this->submitForm([
      'name' => $email,
    ], 'Submit');
    $this->assertLoginFromResetEmail($account);
  }

  /**
   * Tests user with only crowd account can reset password.
   */
  public function testPasswordResetUnprovisionedUser(): void {
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $this->crowdConnector->register($email, $email, $pass, $given_name, $surname, $display_name);
    $this->crowdConnector->updateUserStatus($email);

    // At this point there is no user provisioned in the authmap/externalauth,
    // but the user should still be able to reset their password.
    $this->drupalGet(Url::fromRoute('user.pass'));
    $this->submitForm([
      'name' => $email,
    ], 'Submit');

    $account = user_load_by_mail($email);
    $this->assertInstanceOf(UserInterface::class, $account);

    $this->assertNotNull(\Drupal::service('externalauth.authmap')->get((int) $account->id(), CrowdConnectorInterface::PROVIDER));

    $this->assertLoginFromResetEmail($account);
    $this->assertTrue($account->hasRole($this->verifiedRole->id()));
  }

  /**
   * Tests user with only crowd account can reset password.
   */
  public function testPasswordResetProvisionedLockedUser(): void {
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $account = $this->registerCrowdAccount($email, $pass, $given_name, $surname, $display_name);
    $account->block();
    $account->save();
    $this->drupalGet(Url::fromRoute('user.pass'));
    $this->submitForm([
      'name' => $email,
    ], 'Submit');
    $mails = $this->getMails(['to' => $email]);
    $this->assertCount(0, $mails);
  }

  /**
   * Assert user can login from link in email.
   *
   * @param \Drupal\user\UserInterface $account
   *   Account to login from email link with.
   */
  protected function assertLoginFromResetEmail(UserInterface $account) {
    $email = $account->getEmail();
    $mails = $this->getMails(['to' => $email]);
    $this->assertCount(1, $mails);
    $mail = reset($mails);
    $reset_url = $this->getResetUrl($mail);
    $this->assertNotNull($reset_url);
    $this->drupalGet($reset_url);
    $this->submitForm([], 'Log in');
    // @see ::drupalUserIsLoggedIn()
    $account->sessionId = $this->getSession()
      ->getCookie(\Drupal::service('session_configuration')
        ->getOptions(\Drupal::request())['name']);
    $this->assertTrue($this->drupalUserIsLoggedIn($account));
    return $mails;
  }

}
