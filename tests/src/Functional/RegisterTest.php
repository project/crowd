<?php

declare(strict_types=1);

namespace Drupal\Tests\crowd\Functional;

use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;

/**
 * Defines a class for testing registration.
 *
 * @group crowd
 * @covers \Drupal\crowd\Hooks\Login
 * @covers \Drupal\crowd\Hooks\Register
 * @covers \Drupal\crowd\Hooks\AccountSync
 */
final class RegisterTest extends CrowdTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['text', 'field'];

  /**
   * Field storage.
   *
   * @var \Drupal\field\FieldStorageConfigInterface
   */
  protected FieldStorageConfigInterface $fieldStorage;

  /**
   * Field config.
   *
   * @var \Drupal\Core\Field\FieldConfigInterface
   */
  protected FieldConfigInterface $field;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create an additional field to show on the register form.
    $field_name = strtolower($this->randomMachineName());
    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'user',
      'type' => 'string',
    ]);
    $this->fieldStorage->save();
    $this->field = FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'bundle' => 'user',
    ]);
    $this->field->save();
    $display = \Drupal::service('entity_display.repository')->getFormDisplay('user', 'user', 'register');
    assert($display instanceof EntityFormDisplayInterface);
    $display->setComponent($this->field->getName(), [
      'type' => 'string_textfield',
    ])->save();
  }

  /**
   * Tests registration as a user.
   */
  public function testRegisterAsUser(): void {
    $this->drupalGet('/user/register');
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $field_value = $this->randomMachineName();
    $this->submitForm([
      'mail' => $email,
      'given_name[0][value]' => $given_name,
      'surname[0][value]' => $surname,
      'display_name[0][value]' => $display_name,
      sprintf('%s[0][value]', $this->field->getName()) => $field_value,
    ], 'Create new account');
    $user = user_load_by_mail($email);
    $this->assertInstanceOf(UserInterface::class, $user);
    $this->assertTrue($user->isActive());
    $this->assertEquals($field_value, $user->get($this->field->getName())->value);
    $this->assertEquals($given_name, $user->get('given_name')->value);
    $this->assertEquals($surname, $user->get('surname')->value);
    $this->assertEquals($display_name, $user->get('display_name')->value);
    $new_display_name = $this->randomMachineName();
    $user->set('display_name', $new_display_name)->save();
    $remote_user = $this->crowdConnector->getUser($email);
    $this->assertNotNull($remote_user);
    $this->assertFalse($remote_user->isActive());
    $mails = $this->getMails(['to' => $email]);
    $this->assertCount(1, $mails);
    $mail = reset($mails);
    $reset_url = $this->getResetUrl($mail);
    $this->assertNotNull($reset_url);
    $this->drupalGet($reset_url);
    $this->submitForm([], 'Log in');
    $this->assertSession()->pageTextContains('You have just used your one-time login link. It is no longer necessary to use this link to log in. It is recommended that you set your password.');
    $remote_user = $this->crowdConnector->getUser($email);
    $this->assertNotNull($remote_user);
    $this->assertTrue($remote_user->isActive());
    // Reload user, display name should have synced from crowd.
    $user = \Drupal::entityTypeManager()->getStorage('user')->loadUnchanged($user->id());
    assert($user instanceof UserInterface);
    $this->assertEquals($display_name, $user->get('display_name')->value);
    $this->assertTrue($user->hasRole($this->verifiedRole->id()));
  }

  /**
   * Tests registration as an admin with notify.
   */
  public function testRegisterAsAdminWithNotify(): void {
    $this->drupalLogin($this->drupalCreateUser([
      'administer users',
      'access administration pages',
    ]));
    $this->drupalGet('/admin/people/create');
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $this->submitForm([
      'mail' => $email,
      'name' => $email,
      'notify' => TRUE,
      'given_name[0][value]' => $given_name,
      'surname[0][value]' => $surname,
      'display_name[0][value]' => $display_name,
      'pass[pass1]' => $pass,
      'pass[pass2]' => $pass,
    ], 'Create new account');
    $user = user_load_by_mail($email);
    $this->assertInstanceOf(UserInterface::class, $user);
    $this->assertTrue($user->isActive());
    $this->assertEquals($given_name, $user->get('given_name')->value);
    $this->assertEquals($surname, $user->get('surname')->value);
    $this->assertEquals($display_name, $user->get('display_name')->value);
    $new_display_name = $this->randomMachineName();
    $user->set('display_name', $new_display_name)->save();
    $remote_user = $this->crowdConnector->getUser($email);
    $this->assertNotNull($remote_user);
    $this->assertFalse($remote_user->isActive());
    $mails = $this->getMails(['to' => $email]);
    $this->assertCount(1, $mails);
    $mail = reset($mails);
    $reset_url = $this->getResetUrl($mail);
    $this->assertNotNull($reset_url);
    $this->drupalLogout();

    $this->drupalGet($reset_url);
    $this->submitForm([], 'Log in');
    $this->assertSession()->pageTextContains('You have just used your one-time login link. It is no longer necessary to use this link to log in. It is recommended that you set your password.');
    $remote_user = $this->crowdConnector->getUser($email);
    $this->assertNotNull($remote_user);
    $this->assertTrue($remote_user->isActive());
    // Reload user, display name should have synced from crowd.
    $user = \Drupal::entityTypeManager()->getStorage('user')->loadUnchanged($user->id());
    $this->assertEquals($display_name, $user->get('display_name')->value);
  }

  /**
   * Tests registration as an admin without notify.
   */
  public function testRegisterAsAdminWithoutNotify(): void {
    $this->drupalLogin($this->drupalCreateUser([
      'administer users',
      'access administration pages',
    ]));
    $this->drupalGet('/admin/people/create');
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $this->submitForm([
      'mail' => $email,
      'name' => $email,
      'notify' => FALSE,
      'given_name[0][value]' => $given_name,
      'surname[0][value]' => $surname,
      'display_name[0][value]' => $display_name,
      'pass[pass1]' => $pass,
      'pass[pass2]' => $pass,
    ], 'Create new account');
    $user = user_load_by_mail($email);
    $this->assertInstanceOf(UserInterface::class, $user);
    $this->assertTrue($user->isActive());
    $this->assertEquals($given_name, $user->get('given_name')->value);
    $this->assertEquals($surname, $user->get('surname')->value);
    $this->assertEquals($display_name, $user->get('display_name')->value);
    $new_display_name = $this->randomMachineName();
    $user->set('display_name', $new_display_name)->save();
    $remote_user = $this->crowdConnector->getUser($email);
    $this->assertNotNull($remote_user);
    $this->assertFalse($remote_user->isActive());
    $mails = $this->getMails(['to' => $email]);
    $this->assertCount(0, $mails);
    $this->drupalLogout();

    $this->drupalGet('user/password');
    $this->submitForm(['name' => $email], 'Submit');

    $mails = $this->getMails(['to' => $email]);
    $this->assertCount(1, $mails);
    $mail = reset($mails);
    $reset_url = $this->getResetUrl($mail);
    $this->assertNotNull($reset_url);

    $this->drupalGet($reset_url);
    $this->submitForm([], 'Log in');
    $this->assertSession()->pageTextContains('You have just used your one-time login link. It is no longer necessary to use this link to log in. It is recommended that you set your password.');
    $remote_user = $this->crowdConnector->getUser($email);
    $this->assertNotNull($remote_user);
    $this->assertTrue($remote_user->isActive());
    // Reload user, display name should have synced from crowd.
    $user = \Drupal::entityTypeManager()->getStorage('user')->loadUnchanged($user->id());
    $this->assertEquals($display_name, $user->get('display_name')->value);
  }

  /**
   * Tests registration a local accounts an admin.
   */
  public function testRegisterLocalAccountAsAdmin(): void {
    $this->drupalLogin($this->drupalCreateUser([
      'administer users',
      'access administration pages',
      'administer permissions',
    ]));
    $role = Role::create([
      'id' => mb_strtolower($this->randomMachineName()),
      'label' => $this->randomMachineName(),
    ]);
    assert($role instanceof RoleInterface);
    $role->grantPermission('login to local drupal account');
    $role->save();
    $this->drupalGet('/admin/people/create');
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $this->submitForm([
      'mail' => $email,
      'name' => $email,
      sprintf('roles[%s]', $role->id()) => TRUE,
      'notify' => FALSE,
      'local_account' => TRUE,
      'given_name[0][value]' => $given_name,
      'surname[0][value]' => $surname,
      'display_name[0][value]' => $display_name,
      'pass[pass1]' => $pass,
      'pass[pass2]' => $pass,
    ], 'Create new account');
    $user = user_load_by_mail($email);
    $this->assertInstanceOf(UserInterface::class, $user);
    $this->assertTrue($user->isActive());
    $this->assertEquals($given_name, $user->get('given_name')->value);
    $this->assertEquals($surname, $user->get('surname')->value);
    $this->assertEquals($display_name, $user->get('display_name')->value);
    $remote_user = $this->crowdConnector->getUser($email);
    $this->assertNull($remote_user);
    $this->drupalLogout();

    $this->drupalGet('user/login');
    $user->passRaw = $pass;
    $this->drupalLogin($user);
  }

}
