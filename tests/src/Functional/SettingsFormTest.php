<?php

declare(strict_types=1);

namespace Drupal\Tests\crowd\Functional;

use Drupal\Core\Url;
use Drupal\crowd\Form\SettingsForm;

/**
 * Defines a class for testing settings form.
 *
 * @group crowd
 * @covers
 */
final class SettingsFormTest extends CrowdTestBase {

  /**
   * Tests the setting form.
   */
  public function testSettingsForm(): void {
    $url = Url::fromRoute('crowd.settings');
    $this->drupalGet($url);
    $assert = $this->assertSession();
    $assert->statusCodeEquals(403);

    $this->drupalLogin($this->drupalCreateUser(['administer crowd']));
    $this->drupalGet($url);
    $assert->statusCodeEquals(200);

    $restricted_domain_1 = sprintf('%s.dev.null', $this->randomMachineName());
    $restricted_domain_2 = sprintf('%s.dev.null', $this->randomMachineName());
    $username = $this->randomMachineName();
    $server_uri = sprintf('https://%s.dev.null/crowd', mb_strtolower($this->randomMachineName()));
    $email_as_username = TRUE;
    $verified_role = $this->verifiedRole->id();
    $domains = <<<DOMAINS
$restricted_domain_1
$restricted_domain_2
DOMAINS;

    $this->submitForm([
      'username' => $username,
      'server_uri' => $server_uri,
      'email_as_username' => $email_as_username,
      'verified_role' => $verified_role,
      'restricted_domains' => $domains,
    ], 'Save configuration');
    $config = \Drupal::config(SettingsForm::CROWD_SETTINGS);
    $this->assertEquals($username, $config->get('username'));
    $this->assertEquals($server_uri, $config->get('server_uri'));
    $this->assertEquals($email_as_username, $config->get('email_as_username'));
    $this->assertEquals($verified_role, $config->get('verified_role'));
    $this->assertEquals([$restricted_domain_1, $restricted_domain_2], $config->get('restricted_domains'));
  }

}
