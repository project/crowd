<?php

declare(strict_types=1);

namespace Drupal\Tests\crowd\Functional;

use Drupal\Core\Url;
use Drupal\crowd\Crowd\CrowdConnectorInterface;
use Drupal\crowd_test\Middleware;
use Drupal\user\UserInterface;

/**
 * Defines a class for testing login functionality.
 *
 * @group crowd
 */
final class LoginTest extends CrowdTestBase {

  /**
   * Tests user with an account in crowd can sign in to site.
   */
  public function testLoginWithUnprovisionedAccount(): void {
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $this->crowdConnector->register($email, $email, $pass, $given_name, $surname, $display_name);
    $this->crowdConnector->updateUserStatus($email);

    // At this point there is no user provisioned in the authmap/externalauth,
    // but the user should still be able to log in.
    $this->drupalGet(Url::fromRoute('user.login'));
    $this->submitForm([
      'name' => $email,
      'pass' => $pass,
    ], 'Log in');

    $this->assertSession()->pageTextNotContains('Unrecognized username or password');
    $account = user_load_by_mail($email);
    $this->assertInstanceOf(UserInterface::class, $account);

    $this->assertNotNull(\Drupal::service('externalauth.authmap')->get((int) $account->id(), CrowdConnectorInterface::PROVIDER));

    // @see ::drupalUserIsLoggedIn()
    $account->sessionId = $this->getSession()->getCookie(\Drupal::service('session_configuration')->getOptions(\Drupal::request())['name']);
    $this->assertTrue($this->drupalUserIsLoggedIn($account));

    $this->loggedInUser = $account;
    $this->container->get('current_user')->setAccount($account);
    $account = \Drupal::entityTypeManager()->getStorage('user')->loadUnchanged($account->id());
    $this->assertTrue($account->hasRole($this->verifiedRole->id()));
  }

  /**
   * Tests user once deleted from Crowd does not cause a fatal error.
   */
  public function testLoginWithRemovedCrowdAccount(): void {
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $this->registerCrowdAccount($email, $pass, $given_name, $surname, $display_name);

    // At this point there is both a provisioned Crowd user and a linked Drupal
    // account with a matching authmap entry.
    // Simulate the crowd account being removed by flushing the key value store.
    \Drupal::keyValue(Middleware::KEY_VALUE_STORE_KEY)->deleteAll();

    $this->drupalGet(Url::fromRoute('user.login'));
    $this->submitForm([
      'name' => $email,
      'pass' => $pass,
    ], 'Log in');

    $this->assertSession()->pageTextContains('Authentication with Atlassian Crowd failed');
    $this->assertSession()->statusCodeEquals(200);
  }

}
