<?php

declare(strict_types=1);

namespace Drupal\Tests\crowd\Functional;

use Drupal\crowd\Form\SettingsForm;

/**
 * Defines a class for testing profile form changes.
 *
 * @group crowd
 * @covers \Drupal\crowd\Hooks\Profile
 */
final class ProfileFormTest extends CrowdTestBase {

  /**
   * Test cannot email address.
   */
  public function testEditingEmailNotPossibleIfUsername(): void {
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $account = $this->registerCrowdAccount($email, $pass, $given_name, $surname, $display_name);
    $this->drupalLogin($account);
    $this->drupalGet($account->toUrl('edit-form'));
    $this->assertSession()->fieldNotExists('mail');
    // We need to use named exact because fieldNotExists matches on the 'name'
    // in the given name field.
    $this->assertSession()->elementNotExists('named_exact', ['field', 'name']);
  }

  /**
   * Test editing email address.
   */
  public function testEditingEmail(): void {
    $this->config(SettingsForm::CROWD_SETTINGS)
      ->set('email_as_username', FALSE)
      ->save();
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $username = $this->randomMachineName();
    $account = $this->registerCrowdAccount($email, $pass, $given_name, $surname, $display_name, $username);
    $this->drupalLogin($account);
    $this->assertSession()->fieldNotExists('name');
    $this->drupalGet($account->toUrl('edit-form'));

    // Attempt to update email without adding password.
    $new_email = $this->randomEmail();
    $this->submitForm(['mail' => $new_email], 'Save');
    $assert = $this->assertSession();
    $assert->pageTextContains('Your current password is missing or incorrect');
    $this->submitForm([
      'mail' => $new_email,
      'current_pass' => 'yeh this is not the password',
    ], 'Save');
    $assert->pageTextContains('Your current password is missing or incorrect');
    $remote_user = $this->crowdConnector->getUser($username);
    $this->assertNotNull($remote_user);
    $this->assertEquals($email, $remote_user->getMail());

    $this->submitForm([
      'mail' => $new_email,
      'current_pass' => $pass,
    ], 'Save');
    $assert->pageTextNotContains('Your current password is missing or incorrect');
    $assert->pageTextContains('The changes have been saved.');
    $remote_user = $this->crowdConnector->getUser($username);
    $this->assertNotNull($remote_user);
    $this->assertEquals($new_email, $remote_user->getMail());
  }

  /**
   * Test editing password.
   */
  public function testEditingPassword(): void {
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $account = $this->registerCrowdAccount($email, $pass, $given_name, $surname, $display_name);
    $this->drupalLogin($account);
    $this->drupalGet($account->toUrl('edit-form'));

    // Attempt to update password without current password.
    $new_pass = $this->randomMachineName(15);
    $this->submitForm([
      'pass[pass1]' => $new_pass,
      'pass[pass2]' => $new_pass,
    ], 'Save');
    $assert = $this->assertSession();
    $assert->pageTextContains('Your current password is missing or incorrect');
    $this->submitForm([
      'pass[pass1]' => $new_pass,
      'pass[pass2]' => $new_pass,
      'current_pass' => 'yeh this is not the password',
    ], 'Save');
    $assert->pageTextContains('Your current password is missing or incorrect');

    $this->submitForm([
      'pass[pass1]' => $new_pass,
      'pass[pass2]' => $new_pass,
      'current_pass' => $pass,
    ], 'Save');
    $assert->pageTextNotContains('Your current password is missing or incorrect');
    $assert->pageTextContains('The changes have been saved.');
    $this->drupalLogout();
    $account->passRaw = $new_pass;
    $this->drupalLogin($account);
  }

  /**
   * Test editing password as admin.
   */
  public function testEditingPasswordAdmin(): void {
    $admin = $this->drupalCreateUser([
      'administer users',
      'access user profiles',
    ]);
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $account = $this->registerCrowdAccount($email, $pass, $given_name, $surname, $display_name);
    $this->drupalLogin($admin);
    $this->drupalGet($account->toUrl('edit-form'));

    // Attempt to update password without current password.
    $new_pass = $this->randomMachineName(15);
    $this->submitForm([
      'pass[pass1]' => $new_pass,
      'pass[pass2]' => $new_pass,
    ], 'Save');
    $assert = $this->assertSession();
    $assert->pageTextNotContains('Your current password is missing or incorrect');
    $assert->pageTextContains('The changes have been saved.');

    $this->drupalLogout();
    $account->passRaw = $new_pass;
    $this->drupalLogin($account);
  }

  /**
   * Tests changing email to/from a restricted domain.
   *
   * @dataProvider providerRestricted
   */
  public function testRestrictedDomain(bool $from_restricted): void {
    $this->config(SettingsForm::CROWD_SETTINGS)
      ->set('email_as_username', FALSE)
      ->save();
    $restricted = sprintf('%s@%s', $this->randomMachineName(), $this->restrictedDomain);
    $not_restricted = $this->randomEmail();
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $from_restricted ? $restricted : $not_restricted;
    $pass = $this->randomMachineName(15);
    $account = $this->registerCrowdAccount($email, $pass, $given_name, $surname, $display_name);
    $this->drupalLogin($account);
    $this->drupalGet($account->toUrl('edit-form'));

    // Attempt to update email to/from restricted.
    $new_email = $from_restricted ? $not_restricted : $restricted;
    $this->submitForm([
      'mail' => $new_email,
      'current_pass' => $pass,
    ], 'Save');
    $assert = $this->assertSession();

    $assert->pageTextContains(sprintf('Email address cannot be changed for users from the %s domain.', $this->restrictedDomain));
    $this->assertNull($this->crowdConnector->getUser($new_email));
  }

  /**
   * Tests editing profile fields.
   */
  public function testEditingProfileFields(): void {
    $given_name = $this->randomMachineName();
    $surname = $this->randomMachineName();
    $display_name = $this->randomMachineName();
    $email = $this->randomEmail();
    $pass = $this->randomMachineName(15);
    $account = $this->registerCrowdAccount($email, $pass, $given_name, $surname, $display_name);
    $this->drupalLogin($account);
    $this->drupalGet($account->toUrl('edit-form'));

    $new_name = $this->randomMachineName();
    $this->submitForm(['given_name[0][value]' => $new_name], 'Save');
    $assert = $this->assertSession();
    $assert->pageTextContains('The changes have been saved.');
    $user = $this->crowdConnector->getUser($email);
    $this->assertEquals($new_name, $user->getData()['first-name']);
    $account = \Drupal::entityTypeManager()->getStorage('user')->loadUnchanged($account->id());
    $this->assertEquals($new_name, $account->given_name->value);
  }

  /**
   * Data provider for ::testRestrictedDomain().
   *
   * @return array[]
   *   Test cases.
   */
  public function providerRestricted(): array {
    return [
      'from restricted' => [
        TRUE,
      ],
      'to restricted' => [
        FALSE,
      ],
    ];
  }

}
