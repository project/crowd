<?php

declare(strict_types=1);

namespace Drupal\crowd_test;

use Drupal\Component\Utility\Crypt;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\crowd\Crowd\CrowdConnectorInterface;
use Drupal\crowd\Form\SettingsForm;
use Drupal\key\KeyRepositoryInterface;
use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;

/**
 * Defines a middleware to mock the crowd endpoint.
 */
final class Middleware {

  const KEY_VALUE_STORE_KEY = 'crowd_test';

  /**
   * Key value store.
   */
  protected KeyValueStoreInterface $keyValueStore;

  /**
   * Constructs a new Middleware.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   Key value factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\key\KeyRepositoryInterface $keyRepository
   *   Key repository.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   Uuid.
   */
  public function __construct(
    KeyValueFactoryInterface $keyValueFactory,
    private ConfigFactoryInterface $configFactory,
    private KeyRepositoryInterface $keyRepository,
    private UuidInterface $uuid,
  ) {
    $this->keyValueStore = $keyValueFactory->get(self::KEY_VALUE_STORE_KEY);
  }

  /**
   * Gets username.
   */
  protected function getUsername(): string {
    return $this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('username');
  }

  /**
   * Gets server URI.
   */
  protected function getServerUri(): string {
    return $this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('server_uri');
  }

  /**
   * Gets password.
   */
  protected function getPassword(): ?string {
    return $this->keyRepository->getKey(CrowdConnectorInterface::PASSWORD_KEY_ID)?->getKeyValue();
  }

  /**
   * {@inheritdoc}
   */
  public function __invoke(): callable {
    return function ($nextHandler): callable {
      return function (RequestInterface $request, array $options) use ($nextHandler): PromiseInterface {
        if ($this->shouldInterceptRequest($request)) {
          if ($response = $this->checkRequestAndReturnErrorResponse($request)) {
            return new FulfilledPromise($response);
          }
          return new FulfilledPromise($this->getResponse($request));
        }
        return $nextHandler($request, $options);
      };
    };
  }

  /**
   * Determines if we should intercept the request.
   */
  protected function shouldInterceptRequest(RequestInterface $request): bool {
    return str_starts_with((string) $request->getUri(), $this->getServerUri() . CrowdConnectorInterface::REST_BASE_PATH);
  }

  /**
   * Handlers user end-point.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   Request.
   */
  private function handleUser(RequestInterface $request): Response {
    $query = [];
    parse_str($request->getUri()->getQuery(), $query);
    $user = $query['username'] ?? NULL;
    if ($request->getMethod() === 'GET' && $user) {
      $cid = 'user:' . $user;
      if ($data = $this->keyValueStore->get($cid)) {
        return $this->jsonResponse(data: $data);
      }
      return $this->jsonResponse(404);
    }

    if ($request->getMethod() === 'DELETE' && $user) {
      $cid = 'user:' . $user;
      if ($data = $this->keyValueStore->get($cid)) {
        $restricted_domains = $this->configFactory->get('crowd.settings')->get('restricted_domains');
        [, $domain] = explode('@', $data['email'], 2);
        if (in_array($domain, $restricted_domains, TRUE)) {
          return $this->jsonResponse(403);
        }
        $this->keyValueStore->delete($cid);
        return $this->jsonResponse(204);
      }
      return $this->jsonResponse(404);
    }

    if ($request->getMethod() === 'POST') {
      $json = json_decode($request->getBody()->getContents(), TRUE);
      if (!($json['password']['value'] ?? NULL)) {
        return $this->jsonResponse(400);
      }
      $name = $json['name'] ?? NULL;
      if (!$name) {
        return $this->jsonResponse(400);
      }
      if (!($json['email'] ?? NULL)) {
        return $this->jsonResponse(400);
      }
      $cid = 'user:' . $name;
      if ($this->keyValueStore->get($cid)) {
        return $this->jsonResponse(400);
      }
      $data = [
        'expand' => 'attributes',
        'link' =>
          [
            'href' => 'https://crowdserver/crowd/rest/usermanagement/1/user?username=' . $name,
            'rel' => 'self',
          ],
        'name' => $name,
        'password' =>
          [
            'link' =>
              [
                'href' => 'https://crowdserver/crowd/rest/usermanagement/1/user/password?username=' . $name,
                'rel' => 'edit',
              ],
          ],
        'key' => '557057:' . $this->uuid->generate(),
      ] + $json + [
        'active' => TRUE,
        'first-name' => '',
        'last-name' => '',
        'display-name' => '',
      ];
      $this->keyValueStore->set($cid, $data);
      $this->keyValueStore->set('password:' . $name, password_hash($json['password']['value'], PASSWORD_DEFAULT));
      return $this->jsonResponse(201, $data);
    }

    if ($request->getMethod() === 'PUT' && $user) {
      $json = json_decode($request->getBody()->getContents(), TRUE);
      $name = $json['name'] ?? $user;
      if (!$name) {
        return $this->jsonResponse(400);
      }
      if (!($json['email'] ?? NULL)) {
        return $this->jsonResponse(400);
      }
      $cid = 'user:' . $user;
      if (!$data = $this->keyValueStore->get($cid)) {
        return $this->jsonResponse(404);
      }
      $data = $json + $data;
      if ($name !== $user) {
        $this->keyValueStore->delete($cid);
        $old_password = $this->keyValueStore->get('password:' . $user);
        $this->keyValueStore->set('password:' . $name, $old_password);
      }
      $cid = 'user:' . $name;
      $this->keyValueStore->set($cid, $data);
      return $this->jsonResponse(204);
    }
    return $this->jsonResponse(404);
  }

  /**
   * Handle password responses.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   Request.
   */
  private function handlePassword(RequestInterface $request): Response {
    if ($request->getMethod() !== 'PUT') {
      return $this->jsonResponse(405);
    }
    $query = [];
    parse_str($request->getUri()->getQuery(), $query);
    $user = $query['username'] ?? NULL;
    if (!$user) {
      return $this->jsonResponse(400);
    }
    $cid = 'user:' . $user;
    if (!$this->keyValueStore->get($cid)) {
      return $this->jsonResponse(404);
    }
    $json = json_decode($request->getBody()->getContents(), TRUE);
    if (!($json['value'] ?? NULL)) {
      return $this->jsonResponse(400);
    }
    $this->keyValueStore->set('password:' . $user, password_hash($json['value'], PASSWORD_DEFAULT));
    return $this->jsonResponse(204);
  }

  /**
   * Handle session.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   Request.
   */
  private function handleSession(RequestInterface $request): Response {
    $path = $request->getUri()->getPath();
    $base_path = parse_url($this->getServerUri(), PHP_URL_PATH);
    if (!str_starts_with($path, $base_path . CrowdConnectorInterface::SESSION_PATH)) {
      return $this->jsonResponse(404);
    }
    if ($request->getMethod() === 'POST' && $path === $base_path . CrowdConnectorInterface::SESSION_PATH) {
      $json = json_decode($request->getBody()->getContents(), TRUE);
      $password = $json['password'] ?? NULL;
      $username = $json['username'] ?? NULL;
      $ip_address = $json['validation-factors']['validationFactors'][0] ?? NULL;
      if (($ip_address['name'] ?? NULL) !== 'remote_address' || !filter_var($ip_address['value'] ?? NULL, FILTER_VALIDATE_IP)) {
        return $this->jsonResponse(400);
      }
      if (!$username || !$password) {
        return $this->jsonResponse(400);
      }
      if (!$data = $this->keyValueStore->get('user:' . $username)) {
        return $this->jsonResponse(400);
      }
      if (!$data['active'] ?? FALSE) {
        return $this->jsonResponse(400);
      }
      $stored_password = $this->keyValueStore->get('password:' . $username);
      if (!is_string($stored_password) || $username !== $data['name'] || !password_verify($password, $stored_password)) {
        return $this->jsonResponse(400);
      }
      $token = Crypt::randomBytesBase64();
      $token_data = [
        'expand' => 'user',
        'token' => $token,
        'user' =>
          [
            'name' => $username,
          ],
        'link' =>
          [
            'href' => 'https://crowdserver/crowd/session/' . $token,
            'rel' => 'self',
          ],
        'created-date' => time(),
        'expiry-date' => time() + 3600,
      ];
      $this->keyValueStore->set('user_session:' . $token, $token_data);
      return $this->jsonResponse(201, $token_data);
    }
    if ($request->getMethod() === 'GET') {
      $parts = explode('/', $path);
      $token = end($parts);
      $cid = 'user_session:' . $token;
      if ($data = $this->keyValueStore->get($cid)) {
        return $this->jsonResponse(data: $data);
      }
    }
    if ($request->getMethod() === 'DELETE') {
      $parts = explode('/', $path);
      $token = end($parts);
      $this->keyValueStore->delete('user_session:' . $token);
      return $this->jsonResponse(204);
    }
    return $this->jsonResponse(405);
  }

  /**
   * Gets the response.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   Request.
   */
  private function getResponse(RequestInterface $request): Response {
    $path = $request->getUri()->getPath();
    $base_path = parse_url($this->getServerUri(), PHP_URL_PATH);

    return match ($path) {
      $base_path . CrowdConnectorInterface::USER_PATH => $this->handleUser($request),
      $base_path . CrowdConnectorInterface::PASSWORD_PATH => $this->handlePassword($request),
      $base_path . CrowdConnectorInterface::RESET_PASSWORD_PATH => $this->handlePasswordReset($request),
      // Session has /session/{token} path, so we fallback to that.
      default => $this->handleSession($request),
    };
  }

  /**
   * Builds a JSON response.
   *
   * @param int $code
   *   Response code.
   * @param array $data
   *   Data.
   */
  protected function jsonResponse(int $code = 200, array $data = []): Response {
    return new Response($code, ['Content-type' => 'application/json'], json_encode($data));
  }

  /**
   * Check the request is valid and return an error response if not.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   Request to validate.
   *
   * @return \GuzzleHttp\Psr7\Response|null
   *   NULL if valid otherwise an error response.
   */
  protected function checkRequestAndReturnErrorResponse(RequestInterface $request): ?Response {
    if ($request->getHeaderLine('Accept') !== 'application/json') {
      return new Response(400);
    }
    if ($request->getHeaderLine('Content-type') !== 'application/json') {
      return new Response(400);
    }
    if ($request->getHeaderLine('Connection') !== 'close') {
      return new Response(400);
    }
    $encoded_authorization = base64_encode(sprintf('%s:%s', $this->getUsername(), $this->getPassword()));
    if ($request->getHeaderLine('Authorization') !== sprintf('Basic %s', $encoded_authorization)) {
      return new Response(403);
    }
    return NULL;
  }

  /**
   * Handle password reset.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   Request.
   */
  private function handlePasswordReset(RequestInterface $request): Response {
    if ($request->getMethod() !== 'POST') {
      return $this->jsonResponse(405);
    }
    $query = [];
    parse_str($request->getUri()->getQuery(), $query);
    $user = $query['username'] ?? NULL;
    if (!$user) {
      return $this->jsonResponse(404);
    }
    $cid = 'user:' . $user;
    if (!$this->keyValueStore->get($cid)) {
      return $this->jsonResponse(404);
    }
    return $this->jsonResponse(204);
  }

}
