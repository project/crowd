<?php

declare(strict_types=1);

namespace Drupal\crowd\Hooks;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\crowd\Crowd\CrowdConnectorInterface;
use Drupal\crowd\Form\SettingsForm;
use Drupal\externalauth\AuthmapInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for user verification related hooks.
 */
final class AccountSync implements ContainerInjectionInterface {

  /**
   * Constructs a new Verify.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Route match.
   * @param \Drupal\crowd\Crowd\CrowdConnectorInterface $connector
   *   Connector.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\externalauth\AuthmapInterface $authmap
   *   Authmap service.
   */
  public function __construct(
    private RouteMatchInterface $routeMatch,
    private CrowdConnectorInterface $connector,
    private LoggerChannelInterface $logger,
    private ConfigFactoryInterface $configFactory,
    private EntityTypeManagerInterface $entityTypeManager,
    private AuthmapInterface $authmap,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('current_route_match'),
      $container->get('crowd.connector'),
      $container->get('logger.channel.crowd'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('externalauth.authmap'),
    );
  }

  /**
   * Marks a user as verified and syncs any field values to Drupal if needed.
   *
   * @param \Drupal\user\UserInterface $account
   *   Account to verify.
   */
  public function verifyAndSyncUserIfAppropriate(UserInterface $account): void {
    // If login has succeeded from the one-time password URL, it means the user
    // has verified their email and logged in via a one-time login link.
    if (!$this->authmap->get((int) $account->id(), CrowdConnectorInterface::PROVIDER)) {
      // Not managed by Crowd, nothing to verify.
      return;
    }
    $username = $account->getAccountName();
    if ($this->routeMatch->getRouteName() === 'user.reset.login') {
      if ($this->connector->isVerified($username)) {
        $this->logger->info('User %name is already verified in Crowd', [
          '%name' => $username,
        ]);
        // Ensure the user has the verified role.
        if (($role_id = $this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('verified_role')) &&
          ($role = $this->entityTypeManager->getStorage('user_role')->load($role_id)) &&
          !$account->hasRole($role_id)) {
          $account->addRole($role_id);
          $account->save();
          $this->logger->info('Added %role to Crowd user %name', [
            '%name' => $username,
            '%role' => $role->label(),
          ]);
        }
        return;
      }
      $result = $this->connector->updateUserStatus($username);
      if ($result->isSuccessful()) {
        $this->logger->info('Verified email address for Crowd user %name', ['%name' => $username]);
        if (($role_id = $this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('verified_role')) &&
            $role = $this->entityTypeManager->getStorage('user_role')->load($role_id)) {
          $account->addRole($role_id);
          $this->logger->info('Added %role to Crowd user %name', [
            '%name' => $username,
            '%role' => $role->label(),
          ]);
          $this->updateUserFromRemote($account, $result->getUserAttributes());
        }
        return;
      }
      $this->logger->info('Attempt to verify user %name in Crowd failed', [
        '%name' => $username,
      ]);
      return;
    }
    $remote = $this->connector->getUser($username);
    if ($remote !== NULL) {
      $this->updateUserFromRemote($account, $remote->getData());
    }
  }

  /**
   * Updates a Drupal user using data from Atlassian Crowd response.
   *
   * @param \Drupal\user\UserInterface $user
   *   User to update.
   * @param array $user_data
   *   New user data.
   */
  protected function updateUserFromRemote(UserInterface $user, array $user_data): void {
    $map = [
      'given_name' => 'first-name',
      'surname' => 'last-name',
      'display_name' => 'display-name',
    ];
    $changed = FALSE;
    foreach ($map as $local => $remote) {
      if (!isset($user_data[$remote])) {
        continue;
      }
      $changed = TRUE;
      $user->set($local, $user_data[$remote]);
    }
    if ($changed) {
      $user->save();
    }
  }

}
