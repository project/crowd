<?php

declare(strict_types=1);

namespace Drupal\crowd\Hooks;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\crowd\Crowd\CrowdConnectorInterface;
use Drupal\crowd\Form\SettingsForm;
use Drupal\externalauth\AuthmapInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Defines a class for TFA integration.
 */
final class Tfa implements ContainerInjectionInterface {

  /**
   * Constructs a new Tfa.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   Current user.
   * @param \Drupal\externalauth\AuthmapInterface $authmap
   *   Authmap service.
   * @param \Drupal\crowd\Crowd\CrowdConnectorInterface $connector
   *   Crowd connector.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   */
  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly AccountInterface $currentUser,
    protected readonly AuthmapInterface $authmap,
    protected readonly CrowdConnectorInterface $connector,
    protected readonly ConfigFactoryInterface $configFactory,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('externalauth.authmap'),
      $container->get('crowd.connector'),
      $container->get('config.factory'),
    );
  }

  /**
   * Alter the TFA setup form.
   */
  public function alterForm(array &$form, FormStateInterface $form_state): void {
    array_unshift($form['#validate'], 'crowd_tfa_setup_validate');
    $form['#validate'] = array_diff($form['#validate'], ['::validateForm']);
  }

  /**
   * Validation for the TFA setup form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if (!$this->authmap->get((int) $this->currentUser->id(), CrowdConnectorInterface::PROVIDER)) {
      // This user isn't managed by crowd.
      // Defer to default TFA implementation.
      $form_state->getFormObject()->validateForm($form, $form_state);
      return;
    }
    $values = $form_state->getValues();
    if (!isset($values['current_pass'])) {
      // We're not on step 1 'enter your current password'.
      // Defer to default TFA implementation.
      $form_state->getFormObject()->validateForm($form, $form_state);
      return;
    }
    $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
    assert($user instanceof UserInterface);
    $account = $form['account']['#value'];
    // Allow administrators to change TFA settings for another account using
    // their own password.
    if ((int) $account->id() !== (int) $user->id()) {
      if (!$user->hasPermission('administer tfa for other users')) {
        throw new NotFoundHttpException();
      }
      // TFA allows an admin to set the password for another user, if the admin
      // enters their current password. So we want to check the password of the
      // admin user here.
      $account = $user;
    }
    $username = $account->getAccountName();
    if ($this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('email_as_username')) {
      $username = $account->getEmail();
    }
    $result = $this->connector->login($username, $values['current_pass']);
    if (!$result->isSuccessful()) {
      $form_state->setErrorByName('current_pass', new TranslatableMarkup('Incorrect password.'));
    }
  }

}
