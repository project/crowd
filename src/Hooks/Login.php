<?php

declare(strict_types=1);

namespace Drupal\crowd\Hooks;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\crowd\Crowd\CrowdConnectorInterface;
use Drupal\crowd\Form\SettingsForm;
use Drupal\externalauth\AuthmapInterface;
use Drupal\externalauth\ExternalAuthInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines a class for login hooks.
 */
final class Login implements ContainerInjectionInterface {

  /**
   * Constructs a new Login.
   *
   * @param \Drupal\externalauth\AuthmapInterface $authmap
   *   Auth map.
   * @param \Drupal\externalauth\ExternalAuthInterface $externalAuth
   *   External auth service.
   * @param \Drupal\crowd\Crowd\CrowdConnectorInterface $connector
   *   Crowd connector.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\Core\Password\PasswordGeneratorInterface $passwordGenerator
   *   Password generator.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   */
  public function __construct(
    private AuthmapInterface $authmap,
    private ExternalAuthInterface $externalAuth,
    private CrowdConnectorInterface $connector,
    private EntityTypeManagerInterface $entityTypeManager,
    private RequestStack $requestStack,
    private ConfigFactoryInterface $configFactory,
    private PasswordGeneratorInterface $passwordGenerator,
    private LoggerChannelInterface $logger,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('externalauth.authmap'),
      $container->get('externalauth.externalauth'),
      $container->get('crowd.connector'),
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('config.factory'),
      $container->get('password_generator'),
      $container->get('logger.channel.crowd'),
    );
  }

  /**
   * Finalise login.
   */
  public function loginFinalise(array &$form, FormStateInterface $form_state): void {
    if (!$form_state->get('uid')) {
      // Default Drupal authentication failed, so try alternative means.
      $username = trim($form_state->getValue('name'));
      $user_storage = $this->entityTypeManager->getStorage('user');
      $user = NULL;
      if (($users = $user_storage->loadByProperties(['name' => $username])) ||
        ($users = $user_storage->loadByProperties(['mail' => $username]))) {
        // See if this user is associated with Crowd.
        $user = current($users);
        if (!$this->authmap->get((int) $user->id(), CrowdConnectorInterface::PROVIDER)) {
          // This user exists, but isn't managed by a Crowd login, fall through
          // to default workflow.
          return;
        }
      }

      $password = trim($form_state->getValue('pass'));
      $result = $this->connector->login($username, $password);

      if ($result->isSuccessful()) {
        $attributes = $result->getUserAttributes();
        $form_state->set('crowd_session', $attributes['session']);
        if ($user === NULL) {
          // This is a Crowd account without a provisioned Drupal account.
          $temporary_password = $this->passwordGenerator->generate(50);
          $values = [
            'name' => $result->getName(),
            'mail' => $result->getMail(),
            // Set a random password to prevent issues with hashing and
            // strict-types in PHP 8.1 but also to prevent the local account
            // from being logged in.
            'pass' => $temporary_password,
            'status' => 1,
            'given_name' => $attributes['first-name'] ?? '',
            'surname' => $attributes['last-name'] ?? '',
            'display_name' => $attributes['display-name'] ?? '',
          ];
          $user = $this->externalAuth->register($username, CrowdConnectorInterface::PROVIDER, $values, $attributes);
          if (($role_id = $this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('verified_role')) &&
            $role = $this->entityTypeManager->getStorage('user_role')->load($role_id)) {
            $user->addRole($role_id);
            $user->save();
            $this->logger->info('Added %role to Crowd user %name', [
              '%name' => $username,
              '%role' => $role->label(),
            ]);
          }
        }
        $form_state->set('uid', $user->id());
        $form['#submit'] = [$this, 'loginComplete'];
        return;
      }
      $form_state->setErrorByName('name', new TranslatableMarkup('Authentication with Atlassian Crowd failed.'));
    }
  }

  /**
   * Alters login form.
   */
  public function alterForm(array &$form, FormStateInterface $form_state): void {
    // Add our user login validation in user_login_form after
    // ::validateAuthentication and before ::validateFinal.
    if (in_array('::validateFinal', $form['#validate'] ?? [], TRUE)) {
      $key = array_search('::validateFinal', $form['#validate']);
      $form['#validate'] = array_merge(array_slice($form['#validate'], 0, $key), ['crowd_login_finalise'], array_slice($form['#validate'], $key));
    }
    if ($this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('email_as_username')) {
      $form['name']['#title'] = new TranslatableMarkup('Email address');
      $site_name = $this->configFactory->get('system.site')->get('name');
      $form['name']['#description'] = new TranslatableMarkup('Enter the email address associated with your @s account.', ['@s' => $site_name]);
      $form['pass']['#description'] = new TranslatableMarkup('Enter the password that accompanies your account.');
    }
  }

  /**
   * Submit callback after login.
   */
  public function loginComplete(array &$form, FormStateInterface $form_state) : void {
    if (empty($uid = $form_state->get('uid'))) {
      return;
    }
    $account = $this->entityTypeManager->getStorage('user')->load($uid);
    assert($account instanceof UserInterface);

    if (!$this->requestStack->getMainRequest()->query->has('destination')) {
      $form_state->setRedirectUrl($account->toUrl());
    }
    $this->externalAuth->login($account->getAccountName(), CrowdConnectorInterface::PROVIDER);
    $this->requestStack->getMainRequest()->getSession()->set('crowd', $form_state->get('crowd_session'));
    $form_state->set('crowd_session', NULL);
  }

}
