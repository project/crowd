<?php

declare(strict_types=1);

namespace Drupal\crowd\Hooks;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\crowd\Crowd\CrowdConnectorInterface;
use Drupal\crowd\Form\SettingsForm;
use Drupal\externalauth\ExternalAuthInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for registration related alter hooks.
 */
final class Register implements ContainerInjectionInterface {

  use StringTranslationTrait {
    t as t;
  }
  use DependencySerializationTrait;

  /**
   * Constructs a new RegisterForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\crowd\Crowd\CrowdConnectorInterface $connector
   *   Connector.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\externalauth\ExternalAuthInterface $externalAuth
   *   External auth.
   * @param \Drupal\Core\Password\PasswordGeneratorInterface $passwordGenerator
   *   Password generator.
   */
  public function __construct(
    private ConfigFactoryInterface $configFactory,
    private CrowdConnectorInterface $connector,
    private EntityTypeManagerInterface $entityTypeManager,
    private MessengerInterface $messenger,
    private ExternalAuthInterface $externalAuth,
    private PasswordGeneratorInterface $passwordGenerator,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('crowd.connector'),
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('externalauth.externalauth'),
      $container->get('password_generator'),
    );
  }

  /**
   * Alters form.
   */
  public function alterForm(array &$form, FormStateInterface $form_state): void {
    array_unshift($form['#validate'], 'crowd_user_register_validate');
    // Add a checkbox for making it a Crowd account if admin user is creating.
    $form_class = $form_state->getFormObject();
    assert($form_class instanceof EntityFormInterface);
    $user = $form_class->getEntity();
    $is_admin = $user->access('create');
    $form['local_account'] = [
      '#type' => 'checkbox',
      '#title' => new TranslatableMarkup('Local account'),
      '#access' => $is_admin,
      '#description' => new TranslatableMarkup('Create a local account instead of one in Atlassian Crowd'),
      '#default_value' => $form_state->getValue('local_account', FALSE),
    ];
    // Require an email address.
    $form['account']['mail']['#required'] = TRUE;
    if ($this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('email_as_username')) {
      // Hide the username field for non-admins.
      $form['account']['name']['#access'] = $is_admin;
    }
  }

  /**
   * Validation callback.
   */
  public function validate(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getValue('local_account')) {
      // Nothing to do here.
      return;
    }
    // Bypass the submit handlers in core.
    $form_state->setSubmitHandlers([
      '::submitForm',
      'crowd_user_register_submit',
    ]);
    $form['actions']['submit']['#submit'] = [
      '::submitForm',
      'crowd_user_register_submit',
    ];
    $error_field = 'name';
    $email_as_username = $this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('email_as_username');
    if ($email_as_username) {
      $form_state->setValue('name', $form_state->getValue('mail'));
      $error_field = 'mail';
    }
    $username = $form_state->getValue('name');
    if ($this->connector->userExists($username)) {
      $form_state->setErrorByName($error_field, $this->t('The username %name is already taken.', ['%name' => $username]));
      return;
    }
    $user_storage = $this->entityTypeManager->getStorage('user');
    if ($user_storage->loadByProperties(['mail' => $form_state->getValue('mail')])) {
      $form_state->setErrorByName('mail', $this->t('The email address %mail is already taken.', ['%mail' => $form_state->get('mail')]));
    }
    if (!$email_as_username && $user_storage->loadByProperties(['name' => $username])) {
      $form_state->setErrorByName('name', $this->t('The username %name is already taken.', ['%name' => $username]));
    }
  }

  /**
   * Submit callback.
   */
  public function submit(array &$form, FormStateInterface $form_state): void {
    $notify = !$form_state->isValueEmpty('notify');
    $result = $this->connector->register(
      $form_state->getValue('name'),
      $form_state->getValue('mail'),
      $form_state->getValue('pass'),
      $form_state->getValue(['given_name', 0, 'value'], ''),
      $form_state->getValue(['surname', 0, 'value'], ''),
      $form_state->getValue(['display_name', 0, 'value'], ''),
    );
    if (!$result->isSuccessful()) {
      $this->messenger->addError($this->t('An error occurred registering the account, please try again later'));
      $form_state->setRebuild();
      return;
    }
    $temporary_password = $this->passwordGenerator->generate(50);
    $user_entity = $form_state->getFormObject()->getEntity();
    $extracted = $form_state->get('form_display')->extractFormValues($user_entity, $form, $form_state);
    $values = [
      'name' => $result->getName(),
      'mail' => $result->getMail(),
      // Set a random password to prevent issues with hashing and strict-types
      // in PHP 8.1 but also to prevent the local account from being logged in.
      'pass' => $temporary_password,
      // We need to mark the user as active to use the password reset form/link
      // to verify their email.
      // They won't be able to login because when we create the user in Crowd
      // we set their status as inactive.
      'status' => 1,
      'given_name' => $form_state->getValue(['given_name', 0, 'value'], ''),
      'surname' => $form_state->getValue(['surname', 0, 'value'], ''),
      'display_name' => $form_state->getValue(['display_name', 0, 'value'], ''),
    ];
    foreach (array_diff_key($extracted, $values) as $additional) {
      if ($user_entity->hasField($additional)) {
        $values[$additional] = $user_entity->get($additional)->getValue();
      }
    }
    $account = $this->externalAuth->register($form_state->getValue('name'), CrowdConnectorInterface::PROVIDER, $values, $result->getUserAttributes());
    // Add plain text password into user account to generate mail tokens.
    $account->password = $temporary_password;
    $is_admin = $account->access('create');
    if ($is_admin && !$notify) {
      $this->messenger->addStatus($this->t('Created a new user account for <a href=":url">%name</a>. No email has been sent.', [
        ':url' => $account->toUrl()->toString(),
        '%name' => $account->getAccountName(),
      ]));
      return;
    }
    if ($is_admin) {
      $this->messenger->addStatus($this->t('A welcome message with further instructions has been emailed to the new user <a href=":url">%name</a>.', [
        ':url' => $account->toUrl()->toString(),
        '%name' => $account->getAccountName(),
      ]));
      _user_mail_notify('register_admin_created', $account);
      return;
    }
    $this->messenger->addStatus($this->t('A welcome message with further instructions has been sent to your email address.'));
    _user_mail_notify('register_no_approval_required', $account);
    $form_state->setRedirect('<front>');
  }

}
