<?php

declare(strict_types=1);

namespace Drupal\crowd\Hooks;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\crowd\Crowd\CrowdConnectorInterface;
use Drupal\crowd\Form\SettingsForm;
use Drupal\externalauth\ExternalAuthInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interact with password reset form.
 */
final class PasswordReset implements ContainerInjectionInterface {

  /**
   * Constructs a new PasswordReset.
   *
   * @param \Drupal\externalauth\ExternalAuthInterface $externalAuth
   *   External auth service.
   * @param \Drupal\crowd\Crowd\CrowdConnectorInterface $connector
   *   Crowd connector.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\Core\Password\PasswordGeneratorInterface $passwordGenerator
   *   Password generator.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   */
  public function __construct(
    private ExternalAuthInterface $externalAuth,
    private CrowdConnectorInterface $connector,
    private EntityTypeManagerInterface $entityTypeManager,
    private ConfigFactoryInterface $configFactory,
    private PasswordGeneratorInterface $passwordGenerator,
    private LoggerChannelInterface $logger,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('externalauth.externalauth'),
      $container->get('crowd.connector'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('password_generator'),
      $container->get('logger.channel.crowd'),
    );
  }

  /**
   * Alter the password reset form.
   *
   * @param array $form
   *   Form structure.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   */
  public function alterForm(array &$form, FormStateInterface $formState): void {
    $form['#validate'][] = 'crowd_user_pass_validate';
  }

  /**
   * Validation callback for the user password reset form.
   *
   * @param array $form
   *   Form structure.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   */
  public function validateForm(array &$form, FormStateInterface $formState): void {
    // If core found a user, or the username field is empty, do nothing.
    if ($formState->getValue('account') || !($name = $formState->getValue('name'))) {
      return;
    }
    // Try to load by email.
    $userStorage = $this->entityTypeManager->getStorage('user');
    if ($userStorage->loadByProperties(['mail' => $name]) || $userStorage->loadByProperties(['name' => $name])) {
      // Account exists but is locked.
      return;
    }
    // See if there is a user in crowd.
    if (!($result = $this->connector->getUser($name))) {
      return;
    }
    // We found a user, sync the account to Drupal, but don't send an email.
    $temporary_password = $this->passwordGenerator->generate(50);
    $attributes = $result->getData();
    $values = [
      'name' => $result->getName(),
      'mail' => $result->getMail(),
      'pass' => $temporary_password,
      'status' => 1,
      'given_name' => $attributes['first-name'] ?? '',
      'surname' => $attributes['last-name'] ?? '',
      'display_name' => $attributes['display-name'] ?? '',
    ];
    $user = $this->externalAuth->register($name, CrowdConnectorInterface::PROVIDER, $values, $attributes);
    $role_id = $this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('verified_role');
    if ($role_id &&
      $role = $this->entityTypeManager->getStorage('user_role')->load($role_id)) {
      $user->addRole($role_id);
      $user->save();
      $this->logger->info('Added %role to Crowd user %name', [
        '%name' => $name,
        '%role' => $role->label(),
      ]);
    }
    // Set the account value in the form so the existing submit handler can
    // send the password reset email.
    $formState->setValueForElement(['#parents' => ['account']], $user);
  }

}
