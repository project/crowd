<?php

declare(strict_types=1);

namespace Drupal\crowd\Hooks;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\crowd\Crowd\CrowdConnectorInterface;
use Drupal\crowd\Form\SettingsForm;
use Drupal\externalauth\AuthmapInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for altering profile form.
 */
final class Profile implements ContainerInjectionInterface {

  use StringTranslationTrait {
    t as t;
  }
  use DependencySerializationTrait;

  /**
   * Constructs a new RegisterForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\crowd\Crowd\CrowdConnectorInterface $connector
   *   Connector.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\externalauth\AuthmapInterface $authmap
   *   Authmap.
   * @param \Drupal\Core\Password\PasswordGeneratorInterface $passwordGenerator
   *   Password generator.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   Current user.
   */
  public function __construct(
    private ConfigFactoryInterface $configFactory,
    private CrowdConnectorInterface $connector,
    private EntityTypeManagerInterface $entityTypeManager,
    private MessengerInterface $messenger,
    private AuthmapInterface $authmap,
    private PasswordGeneratorInterface $passwordGenerator,
    private AccountInterface $currentUser,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('crowd.connector'),
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('externalauth.authmap'),
      $container->get('password_generator'),
      $container->get('current_user'),
    );
  }

  /**
   * Alters user profile/edit form.
   */
  public function alterForm(array &$form, FormStateInterface $form_state): void {
    $user = $this->getUser($form_state);
    if (!$this->authmap->get((int) $user->id(), CrowdConnectorInterface::PROVIDER)) {
      return;
    }
    // Require an email address.
    $form['account']['mail']['#required'] = TRUE;
    $config = $this->configFactory->get(SettingsForm::CROWD_SETTINGS);
    // Crowd does not let you change your username once set.
    $form['account']['name']['#access'] = FALSE;
    if ($config->get('email_as_username')) {
      // Crowd does not let you change your username once set. Which with this
      // configuration is the email field.
      $form['account']['mail']['#access'] = FALSE;
    }
    $mail = $user->getEmail();
    $restricted_domains = (array) $config->get('restricted_domains');
    [, $domain] = explode('@', $mail, 2);
    if (in_array($domain, $restricted_domains, TRUE)) {
      $form['account']['mail']['#description'] = $this->t('Email address cannot be changed for users from the %domain domain.', ['%domain' => $domain]);
    }
    // Initial validation needs to go first.
    array_unshift($form['#validate'], 'crowd_user_form_initial_validation');
    // Additional validation to unset any flags around user pass reset need to
    // run immediately after ::validateForm.
    if (in_array('::validateForm', $form['#validate'] ?? [], TRUE)) {
      $key = array_search('::validateForm', $form['#validate']);
      $form['#validate'] = array_merge(array_slice($form['#validate'], 0, $key + 1), ['crowd_user_form_complete_validation'], array_slice($form['#validate'], $key + 1));
    }
  }

  /**
   * Validation callback.
   */
  public function initialValidation(array &$form, FormStateInterface $form_state): void {
    // This only runs if the user is authenticated with Crowd.
    $submit_handlers = $form_state->getSubmitHandlers();
    array_unshift($submit_handlers, 'crowd_user_form_submit');
    $form_state->setSubmitHandlers($submit_handlers);

    $config = $this->configFactory->get(SettingsForm::CROWD_SETTINGS);
    $user = $this->getUser($form_state);
    $original_mail = $user->getEmail();
    $restricted_domains = (array) $config->get('restricted_domains');
    [, $original_domain] = explode('@', $original_mail, 2);
    $new_mail = $form_state->getValue('mail');
    [, $new_domain] = explode('@', $new_mail, 2);
    $email_has_changed = $new_mail !== $original_mail;
    if (in_array($original_domain, $restricted_domains, TRUE) && $email_has_changed) {
      $form_state->setErrorByName('mail', $this->t('Email address cannot be changed for users from the %domain domain.', ['%domain' => $original_domain]));
      return;
    }
    if (in_array($new_domain, $restricted_domains, TRUE) && $email_has_changed) {
      $form_state->setErrorByName('mail', $this->t('Email address cannot be changed for users from the %domain domain.', ['%domain' => $new_domain]));
      return;
    }
    $current_pass = trim($form_state->getValue('current_pass', ''));
    // If changing email or password, require current password - validate it
    // against Crowd API, and if it passes, set the user_pass_reset flag so that
    // the remaining user.module APIs don't attempt to require the dummy value
    // that is stored in the database.
    $new_password = trim($form_state->getValue('pass', ''));
    $password_reset = $form_state->get('user_pass_reset');
    $email_as_username = $this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('email_as_username');
    if ($email_as_username && $email_has_changed) {
      $form_state->setErrorByName('mail', $this->t("You cannot change your email address."));
      return;
    }
    $is_admin = (int) $this->currentUser->id() !== (int) $user->id();
    if ($email_has_changed || ($new_password && !$password_reset)) {
      if (!$current_pass && !$is_admin) {
        $form_state->setErrorByName('current_pass', $this->t("Your current password is missing or incorrect; it's required to change your email address."));
        return;
      }
      // Check the current password.
      if (!$is_admin) {
        $result = $this->connector->login($email_as_username ? $user->getEmail() : $user->getAccountName(), $current_pass);
        if (!$result->isSuccessful()) {
          $form_state->setErrorByName('current_pass', $this->t("Your current password is missing or incorrect; it's required to change your email address or password."));
          return;
        }
      }
      if (!$is_admin) {
        // Flag to the user module that the current password isn't required.
        // @see \Drupal\user\AccountForm::buildEntity
        $form_state->set('initial_user_pass_reset', $form_state->get('user_pass_reset'));
        $form_state->set('user_pass_reset', TRUE);
      }
    }
    // Don't let the user module update the password for the user entity.
    if ($new_password) {
      $form_state->setValue('pass', $this->passwordGenerator->generate(50));
      $form_state->setValue('new_crowd_password', $new_password);
    }
  }

  /**
   * Validation callback.
   */
  public function completeValidation(array &$form, FormStateInterface $form_state): void {
    // Put things back the way they were after core user validation has run.
    $form_state->set('user_pass_reset', $form_state->get('initial_user_pass_reset'));
    $form_state->unsetValue('initial_user_pass_reset');
  }

  /**
   * Submit callback.
   */
  public function submit(array &$form, FormStateInterface $form_state): void {
    // Update user details.
    $user = $this->getUser($form_state);
    $unchanged = $this->entityTypeManager->getStorage('user')->loadUnchanged($user->id());
    assert($unchanged instanceof UserInterface);
    $email_as_username = $this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('email_as_username');
    $original_username = $email_as_username ? $unchanged->getEmail() : $unchanged->getAccountName();
    if ($password = $form_state->getValue('new_crowd_password')) {
      $password_result = $this->connector->updatePassword($original_username, $password);
      if (!$password_result) {
        $message = $this->t('An error occurred updating your password in Atlassian Crowd, please try again later');
        $this->messenger->addError($message);
        $form_state->setRebuild();
        return;
      }
    }
    $result = $this->connector->updateUser(
      $original_username,
      $email_as_username ? $form_state->getValue('mail', '') : $form_state->getValue('name', ''),
      $form_state->getValue('mail', ''),
      $form_state->getValue('pass', ''),
      $form_state->getValue(['given_name', 0, 'value'], ''),
      $form_state->getValue(['surname', 0, 'value'], ''),
      $form_state->getValue(['display_name', 0, 'value'], ''),
    );
    if (!$result->isSuccessful()) {
      $message = $this->t('An error occurred updating your account in Atlassian Crowd, please try again later');
      $this->messenger->addError($message);
      $form_state->setRebuild();
    }
  }

  /**
   * Get user from form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\user\UserInterface
   *   User from form.
   */
  protected function getUser(FormStateInterface $form_state): UserInterface {
    $form_class = $form_state->getFormObject();
    assert($form_class instanceof EntityFormInterface);
    $user = $form_class->getEntity();
    assert($user instanceof UserInterface);
    return $user;
  }

  /**
   * Implements hook_entity_field_access().
   */
  public function fieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    $neutral = AccessResult::neutral();
    if ($field_definition->getTargetEntityTypeId() !== 'user' || $operation !== 'edit') {
      return $neutral;
    }
    $field_name = $field_definition->getName();
    if (!in_array($field_name, [
      'given_name',
      'surname',
      'display_name',
    ], TRUE)) {
      return $neutral;
    }
    if (!$items->getEntity()) {
      return $neutral;
    }
    $restricted_domains = $this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('restricted_domains');
    $entity = $items->getEntity();
    assert($entity instanceof UserInterface);
    if (!str_contains($entity->getEmail(), '@')) {
      return $neutral;
    }
    [, $domain] = explode('@', $entity->getEmail(), 2);
    if (!in_array($domain, $restricted_domains)) {
      return $neutral;
    }
    return AccessResult::forbidden(sprintf('%s cannot be changed for users from the %s domain.', $field_definition->getLabel(), $domain));
  }

}
