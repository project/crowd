<?php

declare(strict_types=1);

namespace Drupal\crowd\Crowd;

/**
 * Defines a class for a Crowd user value object.
 */
final class CrowdUser {

  /**
   * Constructs a new CrowdUser.
   *
   * @param string $name
   *   Username.
   * @param string $mail
   *   User mail.
   * @param array $data
   *   User data.
   */
  private function __construct(
    protected string $name,
    protected string $mail,
    protected array $data = [],
  ) {
  }

  /**
   * Factory method to create from response data.
   *
   * @param array $data
   *   Response data from Crowd.
   *
   * @return static
   */
  public static function fromResponseData(array $data): static {
    return new static($data['name'], $data['email'], $data);
  }

  /**
   * Gets data for user.
   *
   * @return array
   *   User data returned by Crowd.
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * Gets user email.
   *
   * @return string
   *   Email address.
   */
  public function getMail(): string {
    return $this->mail;
  }

  /**
   * Gets username.
   *
   * @return string
   *   Username.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Adds to user-data.
   *
   * @param string $key
   *   Key to set.
   * @param array $data
   *   Value to set for key.
   *
   * @return $this
   */
  public function addData(string $key, array $data): CrowdUser {
    $this->data[$key] = $data;
    return $this;
  }

  /**
   * Sets user as active.
   *
   * @param bool $state
   *   State. TRUE for active, FALSE for inactive.
   *
   * @return $this
   */
  public function setActiveState(bool $state = TRUE): CrowdUser {
    $this->data['active'] = $state;
    return $this;
  }

  /**
   * Checks if user is active.
   *
   * @return bool
   *   TRUE if active.
   */
  public function isActive(): bool {
    return $this->data['active'] ?? FALSE;
  }

}
