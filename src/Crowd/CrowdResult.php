<?php

declare(strict_types=1);

namespace Drupal\crowd\Crowd;

/**
 * Defines a class for a crowd login/authentication response.
 */
final class CrowdResult {

  /**
   * Constructs a new CrowdResult.
   *
   * @param bool $result
   *   TRUE if succeeded.
   * @param string|null $name
   *   Username if known.
   * @param string|null $mail
   *   User email if known.
   * @param array $data
   *   User data if known.
   * @param string|null $reason
   *   Reason for failure if appropriate.
   */
  private function __construct(
    protected bool $result,
    protected ?string $name = NULL,
    protected ?string $mail = NULL,
    protected array $data = [],
    protected ?string $reason = NULL,
  ) {
  }

  /**
   * Factory method that creates a result from a user.
   *
   * @param \Drupal\crowd\Crowd\CrowdUser $crowd_user
   *   User.
   *
   * @return static
   */
  public static function forCrowdUser(CrowdUser $crowd_user): static {
    return new static(TRUE, $crowd_user->getName(), $crowd_user->getMail(), $crowd_user->getData());
  }

  /**
   * Check if the result was a success.
   *
   * @return bool
   *   TRUE if succeeded
   */
  public function isSuccessful(): bool {
    return $this->result;
  }

  /**
   * Factory method to create a result from a failure.
   *
   * @param string $reason
   *   Failure reason.
   *
   * @return static
   */
  public static function failure(string $reason): static {
    return new static(FALSE, reason: $reason);
  }

  /**
   * Gets the user attributes if they exist.
   *
   * @return array
   *   User attributes.
   */
  public function getUserAttributes(): array {
    return $this->data;
  }

  /**
   * Gets the user's email address if it exists.
   *
   * @return string|null
   *   User email.
   */
  public function getMail(): ?string {
    return $this->mail;
  }

  /**
   * Gets the user's name if it exists.
   *
   * @return string|null
   *   User name.
   */
  public function getName(): ?string {
    return $this->name;
  }

  /**
   * Gets value of failure reason if it exists.
   *
   * @return string|null
   *   Value of reason.
   */
  public function getReason(): ?string {
    return $this->reason;
  }

}
