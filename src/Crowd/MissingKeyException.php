<?php

declare(strict_types=1);

namespace Drupal\crowd\Crowd;

/**
 * Defines an exception for a missing key .
 */
final class MissingKeyException extends \Exception {

  /**
   * Factory method to create an exception for a given Key ID.
   *
   * @param string $key_id
   *   Key ID.
   *
   * @return static
   */
  public static function forKeyId(string $key_id): static {
    return new static(sprintf('Could not load key, key %s does not exist', $key_id));
  }

}
