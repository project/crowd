<?php

declare(strict_types=1);

namespace Drupal\crowd\Crowd;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\crowd\Form\SettingsForm;
use Drupal\key\KeyRepositoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides a connector for interacting with Atlassian Crowd.
 */
final class CrowdConnector implements CrowdConnectorInterface {

  /**
   * Constructs a new CrowdConnector.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   HTTP client.
   * @param \Drupal\key\KeyRepositoryInterface $keyRepository
   *   Key repository.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   */
  public function __construct(
    protected ClientInterface $httpClient,
    protected KeyRepositoryInterface $keyRepository,
    protected ConfigFactoryInterface $configFactory,
    protected LoggerChannelInterface $logger,
    protected RequestStack $requestStack,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function updateUserStatus(string $username, bool $status = TRUE): CrowdResult {
    $user = $this->getUser($username);
    if (!$user) {
      $this->logger->error(sprintf('An error occurred whilst verifying user %s, there is no such user in Crowd, please remove the local Drupal account.', $username));
      return CrowdResult::failure('No such user');
    }
    try {
      $response = $this->makeRequest(sprintf('%s?username=%s', self::USER_PATH, urlencode($username)), $user->setActiveState($status)->getData(), 'PUT');
      if ($response->getStatusCode() === Response::HTTP_NO_CONTENT) {
        $this->logger->info(sprintf('User %s set as active in Crowd API', $username));
        return CrowdResult::forCrowdUser($user);
      }
    }
    catch (RequestException $e) {
      $this->logger->error(sprintf('An error occurred whilst verifying user %s, the message was %s. The request body was: <pre><code>%s</code></pre>', $username, $e->getMessage(), $e->getResponse()->getBody()));
      return CrowdResult::failure('Registration failed');
    }
    catch (\Exception $e) {
      $this->logger->error(sprintf('An error occurred whilst verifying user %s, the message was %s', $username, $e->getMessage()));
      return CrowdResult::failure('An error occurred during verification');
    }
    $this->logger->error(sprintf('The crowd server provided an unexpected response whilst registering user %s, the status code was %s. The request body was: <pre><code>%s</code></pre>', $username, $response->getStatusCode(), $response->getBody()));
    return CrowdResult::failure('Crowd returned an unexpected response');
  }

  /**
   * {@inheritdoc}
   */
  public function register(
    string $username,
    string $mail,
    #[\SensitiveParameter]
    string $password,
    string $given_name,
    string $surname,
    string $display_name = '',
  ): CrowdResult {
    try {
      $response = $this->makeRequest(self::USER_PATH, [
        'name' => $username,
        'password' => ['value' => trim($password)],
        // User starts out inactive until they verify their email. At present,
        // this doesn't work - see https://jira.atlassian.com/browse/CWD-3018
        // and code below.
        'active' => FALSE,
        'first-name' => $given_name,
        'last-name' => $surname,
        'display-name' => $display_name ?: sprintf('%s %s', $given_name, $surname),
        'email' => $mail,
      ], 'POST');
      if ($response->getStatusCode() === Response::HTTP_CREATED) {
        $data = json_decode((string) $response->getBody(), TRUE);
        $this->logger->info(sprintf('User %s registered via Crowd API', $username));
        // By default, Crowd creates users with active = true. We cannot change
        // this, so after we create the user we update it to active = false.
        // See: https://jira.atlassian.com/browse/CWD-3018
        // The user needs to click the activation link in the email to set mark
        // the account active.
        $this->updateUserStatus($username, FALSE);
        return CrowdResult::forCrowdUser(CrowdUser::fromResponseData($data));
      }
    }
    catch (RequestException $e) {
      $requestBody = $e->getResponse()?->getBody() ?? '- No request body -';
      $this->logger->error(sprintf('An error occurred whilst registering user %s, the message was %s. The request body was: <pre><code>%s</code></pre>', $username, $e->getMessage(), $requestBody));
      return CrowdResult::failure('Registration failed');
    }
    catch (\Exception $e) {
      $this->logger->error(sprintf('An error occurred whilst registering user %s, the message was %s', $username, $e->getMessage()));
      return CrowdResult::failure('An error occurred during registration');
    }
    $this->logger->error(sprintf('The crowd server provided an unexpected response whilst registering user %s, the status code was %s. The request body was: <pre><code>%s</code></pre>', $username, $response->getStatusCode(), $response->getBody()));
    return CrowdResult::failure('Crowd returned an unexpected response');
  }

  /**
   * {@inheritdoc}
   */
  public function login(string $username, string $password): CrowdResult {
    try {
      $response = $this->makeRequest(self::SESSION_PATH, [
        'username' => $username,
        'password' => trim($password),
        'validation-factors' => [
          'validationFactors' => [
            [
              'name' => 'remote_address',
              'value' => $this->requestStack->getMainRequest()->getClientIp(),
            ],
          ],
        ],
      ], 'POST');
      if ($response->getStatusCode() === Response::HTTP_CREATED) {
        $data = json_decode((string) $response->getBody(), TRUE);
        if ($user_data = $this->getUser($username)) {
          $this->logger->info(sprintf('User %s logged in via Crowd API', $username));
          return CrowdResult::forCrowdUser($user_data->addData('session', $data));
        }
        return CrowdResult::failure('Login was successful, but the user could not be found.');
      }
    }
    catch (RequestException $e) {
      $this->logger->notice(sprintf('An error occurred whilst logging in user %s, the message was %s. The request body was: <pre><code>%s</code></pre>', $username, $e->getMessage(), $e->getResponse()->getBody()));
      return CrowdResult::failure('Login failed');
    }
    catch (\Exception $e) {
      $this->logger->error(sprintf('An error occurred whilst logging in user %s, the message was %s', $username, $e->getMessage()));
      return CrowdResult::failure('An error occurred during login');
    }
    $this->logger->error(sprintf('The crowd server provided an unexpected response whilst logging in user %s, the status code was %s. The request body was: <pre><code>%s</code></pre>', $username, $response->getStatusCode(), $response->getBody()));
    return CrowdResult::failure('Crowd returned an unexpected response');
  }

  /**
   * {@inheritdoc}
   */
  public function logout(string $username, string $session_token): bool {
    try {
      $response = $this->makeRequest(sprintf('%s/%s', self::SESSION_PATH, $session_token), method: 'DELETE');
      if ($response->getStatusCode() === Response::HTTP_NO_CONTENT) {
        $this->logger->info(sprintf('User %s logged out via Crowd API', $username));
        return TRUE;
      }
    }
    catch (RequestException $e) {
      $this->logger->error(sprintf('An error occurred whilst logging out user %s, the message was %s. The request body was: <pre><code>%s</code></pre>', $username, $e->getMessage(), $e->getResponse()->getBody()));
      return FALSE;
    }
    catch (\Exception $e) {
      $this->logger->error(sprintf('An error occurred whilst logging out user %s, the message was %s', $username, $e->getMessage()));
      return FALSE;
    }
    $this->logger->error(sprintf('The crowd server provided an unexpected response whilst logging out user %s, the status code was %s. The request body was: <pre><code>%s</code></pre>', $username, $response->getStatusCode(), $response->getBody()));
    return FALSE;
  }

  /**
   * Makes a HTTP request.
   *
   * @param string $path
   *   Path to request.
   * @param array $data
   *   Request body.
   * @param string $method
   *   HTTP method.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response.
   *
   * @throws \Drupal\crowd\Crowd\MissingKeyException
   *   When there is no key for the user password.
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   When the request fails.
   */
  protected function makeRequest(string $path, array $data = [], string $method = 'GET'): ResponseInterface {
    return $this->httpClient->request($method, $this->buildUri($path), array_filter([
      'json' => $data,
      'headers' => $this->getHeaders(),
      'auth' => $this->getAuthentication(),
    ]));
  }

  /**
   * Gets headers to add to the request.
   *
   * @return string[]
   *   Array of requst headers.
   */
  protected function getHeaders(): array {
    return [
      'Content-type' => 'application/json',
      'Accept' => 'application/json',
      'Connection' => 'close',
    ];
  }

  /**
   * Gets the authentication (username/password) to use with the request.
   *
   * @return array
   *   Array with username as first entry, and password as second.
   *
   * @throws \Drupal\crowd\Crowd\MissingKeyException
   *   When there is no available key for password storage.
   */
  protected function getAuthentication(): array {
    $key = $this->keyRepository->getKey(self::PASSWORD_KEY_ID);
    if (!$key) {
      $this->logger->critical(sprintf('Could not load password, key %s does not exist', self::PASSWORD_KEY_ID));
      throw MissingKeyException::forKeyId(self::PASSWORD_KEY_ID);
    }
    return [
      $this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('username'),
      $key->getKeyValue(),
    ];
  }

  /**
   * Builds URI for the request.
   *
   * @param string $path
   *   Path to request.
   *
   * @return string
   *   Fully-qualified URI for the given path.
   */
  protected function buildUri(string $path): string {
    return sprintf('%s%s', $this->configFactory->get(SettingsForm::CROWD_SETTINGS)->get('server_uri'), $path);
  }

  /**
   * {@inheritdoc}
   */
  public function getUser(string $username): ?CrowdUser {
    try {
      $response = $this->makeRequest(sprintf('%s?username=%s', self::USER_PATH, urlencode($username)));
      if ($response->getStatusCode() === Response::HTTP_OK) {
        $data = json_decode((string) $response->getBody(), TRUE);
        return CrowdUser::fromResponseData($data);
      }
    }
    catch (RequestException $e) {
      $this->logger->warning(sprintf('No such user %s, the message was %s. The request body was: <pre><code>%s</code></pre>', $username, $e->getMessage(), $e->getResponse()->getBody()));
      return NULL;
    }
    catch (\Exception $e) {
      $this->logger->error(sprintf('An occurred whilst getting user details for %s, the message was %s', $username, $e->getMessage()));
      return NULL;
    }
    $this->logger->error(sprintf('The crowd server provided an unexpected response whilst fetching user %s, the status code was %s. The request body was: <pre><code>%s</code></pre>', $username, $response->getStatusCode(), $response->getBody()));
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function userExists(string $username): bool {
    try {
      $response = $this->makeRequest(sprintf('%s?username=%s', self::USER_PATH, urlencode($username)));
      if ($response->getStatusCode() === Response::HTTP_OK) {
        return TRUE;
      }
      return FALSE;
    }
    catch (\Exception) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function passwordReset(string $username): bool {
    try {
      $response = $this->makeRequest(sprintf('%s?username=%s', self::RESET_PASSWORD_PATH, urlencode($username)));
      if ($response->getStatusCode() === Response::HTTP_NO_CONTENT) {
        return TRUE;
      }
    }
    catch (RequestException $e) {
      $this->logger->warning(sprintf('No such user or access denied on reset password for %s, the message was %s. The request body was: <pre><code>%s</code></pre>', $username, $e->getMessage(), $e->getResponse()->getBody()));
      return FALSE;
    }
    catch (\Exception $e) {
      $this->logger->error(sprintf('An occurred whilst resetting password for %s, the message was %s', $username, $e->getMessage()));
      return FALSE;
    }
    $this->logger->error(sprintf('The crowd server provided an unexpected response whilst performing a password reset for user %s, the status code was %s. The request body was: <pre><code>%s</code></pre>', $username, $response->getStatusCode(), $response->getBody()));
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isVerified(string $username): bool {
    $user = $this->getUser($username);
    return $user && $user->isActive();
  }

  /**
   * {@inheritdoc}
   */
  public function updateUser(string $original_username, string $username, string $mail, string $password, string $given_name, string $surname, string $display_name = ''): CrowdResult {
    try {
      $response = $this->makeRequest(sprintf('%s?username=%s', self::USER_PATH, urlencode($original_username)), array_filter([
        'name' => $username,
        'password' => $password ? ['value' => trim($password)] : NULL,
        'first-name' => $given_name,
        'last-name' => $surname,
        'display-name' => $display_name,
        'email' => $mail,
      ]), 'PUT');
      if ($response->getStatusCode() === Response::HTTP_NO_CONTENT) {
        $new_username = $username ?: $original_username;
        $new_user = $this->getUser($new_username);
        if (!$new_user) {
          $this->logger->error(sprintf('An error occurred whilst updating user %s, the user did not exist after update', $original_username));
          return CrowdResult::failure('An error occurred during updating user');
        }
        $this->logger->info(sprintf('User %s updated in Crowd API', $username));
        return CrowdResult::forCrowdUser($new_user);
      }
    }
    catch (RequestException $e) {
      $this->logger->error(sprintf('An error occurred whilst updating user %s, the message was %s. The request body was: <pre><code>%s</code></pre>', $original_username, $e->getMessage(), $e->getResponse()->getBody()));
      return CrowdResult::failure('Update failed');
    }
    catch (\Exception $e) {
      $this->logger->error(sprintf('An error occurred whilst updating user %s, the message was %s', $original_username, $e->getMessage()));
      return CrowdResult::failure('An error occurred during updating user');
    }
    $this->logger->error(sprintf('The crowd server provided an unexpected response whilst updating user %s, the status code was %s. The request body was: <pre><code>%s</code></pre>', $original_username, $response->getStatusCode(), $response->getBody()));
    return CrowdResult::failure('Crowd returned an unexpected response');
  }

  /**
   * {@inheritdoc}
   */
  public function updatePassword(string $username, string $password): bool {
    try {
      $response = $this->makeRequest(sprintf('%s?username=%s', self::PASSWORD_PATH, urlencode($username)), [
        'value' => $password,
      ], 'PUT');
      if ($response->getStatusCode() === Response::HTTP_NO_CONTENT) {
        $this->logger->info(sprintf('User %s updated password in Crowd API', $username));
        return TRUE;
      }
    }
    catch (RequestException $e) {
      $this->logger->error(sprintf('An error occurred whilst updating password for %s, the message was %s. The request body was: <pre><code>%s</code></pre>', $username, $e->getMessage(), $e->getResponse()->getBody()));
      return FALSE;
    }
    catch (\Exception $e) {
      $this->logger->error(sprintf('An error occurred whilst updating password for %s, the message was %s', $username, $e->getMessage()));
      return FALSE;
    }
    $this->logger->error(sprintf('The crowd server provided an unexpected response whilst updating password for %s, the status code was %s. The request body was: <pre><code>%s</code></pre>', $username, $response->getStatusCode(), $response->getBody()));
    return FALSE;
  }

}
