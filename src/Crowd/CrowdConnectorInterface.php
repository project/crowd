<?php

declare(strict_types=1);

namespace Drupal\crowd\Crowd;

/**
 * Defines a class for a Crowd connector service.
 */
interface CrowdConnectorInterface {
  const REST_BASE_PATH = '/rest';
  const USER_MANAGEMENT_PATH = self::REST_BASE_PATH . '/usermanagement/1';
  const USER_PATH = self::USER_MANAGEMENT_PATH . '/user';
  const PASSWORD_PATH = self::USER_MANAGEMENT_PATH . '/user/password';
  const SESSION_PATH = self::USER_MANAGEMENT_PATH . '/session';
  const RESET_PASSWORD_PATH = self::USER_MANAGEMENT_PATH . '/user/mail/password';
  const PASSWORD_KEY_ID = 'crowd_password';
  const PROVIDER = 'crowd';

  /**
   * Log a user in.
   *
   * @param string $username
   *   Username.
   * @param string $password
   *   Password.
   *
   * @return \Drupal\crowd\Crowd\CrowdResult
   *   Result of the login attempt.
   */
  public function login(string $username, string $password): CrowdResult;

  /**
   * Send a password reset email for the given username.
   *
   * @param string $username
   *   Username.
   *
   * @return bool
   *   TRUE if succeeded
   */
  public function passwordReset(string $username): bool;

  /**
   * Get user details for the given username.
   *
   * @param string $username
   *   Username of user being retrieved.
   *
   * @return \Drupal\crowd\Crowd\CrowdUser|null
   *   Either the user if one exists with the given name, or NULL.
   */
  public function getUser(string $username): ?CrowdUser;

  /**
   * Register a new user.
   *
   * @param string $username
   *   Username.
   * @param string $mail
   *   User's email.
   * @param string $password
   *   User's password.
   * @param string $given_name
   *   User's given name.
   * @param string $surname
   *   User's surname.
   * @param string $display_name
   *   User's display name.
   *
   * @return \Drupal\crowd\Crowd\CrowdResult
   *   Result of the registration attempt.
   */
  public function register(string $username, string $mail, string $password, string $given_name, string $surname, string $display_name = ''): CrowdResult;

  /**
   * Update a user.
   *
   * @param string $original_username
   *   Original username.
   * @param string $username
   *   Username.
   * @param string $mail
   *   User's email.
   * @param string $password
   *   User's password.
   * @param string $given_name
   *   User's given name.
   * @param string $surname
   *   User's surname.
   * @param string $display_name
   *   User's display name.
   *
   * @return \Drupal\crowd\Crowd\CrowdResult
   *   Result of the update operation.
   */
  public function updateUser(string $original_username, string $username, string $mail, string $password, string $given_name, string $surname, string $display_name = ''): CrowdResult;

  /**
   * Logs a user out.
   *
   * @param string $username
   *   Username to logout.
   * @param string $session_token
   *   Session token to expire.
   *
   * @return bool
   *   TRUE if succeeded.
   */
  public function logout(string $username, string $session_token): bool;

  /**
   * Verifies a user.
   *
   * @param string $username
   *   Username to verify.
   * @param bool $status
   *   User status, TRUE for active, FALSE for inactive.
   *
   * @return \Drupal\crowd\Crowd\CrowdResult
   *   Result of the operation.
   */
  public function updateUserStatus(string $username, bool $status = TRUE): CrowdResult;

  /**
   * Check if a user exists.
   *
   * @param string $username
   *   Username to check.
   *
   * @return bool
   *   TRUE if a user exists with the given username.
   */
  public function userExists(string $username): bool;

  /**
   * Checks if a user has verified their email address.
   *
   * @param string $username
   *   Username to check.
   *
   * @return bool
   *   TRUE if the user with the given username is verified.
   */
  public function isVerified(string $username): bool;

  /**
   * Updates the users's password.
   *
   * @param string $username
   *   Username.
   * @param string $password
   *   Password.
   *
   * @return bool
   *   True if succeeded.
   */
  public function updatePassword(string $username, string $password): bool;

}
