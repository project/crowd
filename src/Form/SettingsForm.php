<?php

declare(strict_types=1);

namespace Drupal\crowd\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a settings form for the module.
 */
final class SettingsForm extends ConfigFormBase {

  const CROWD_SETTINGS = 'crowd.settings';

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [self::CROWD_SETTINGS];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return self::CROWD_SETTINGS;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->configFactory()->get(self::CROWD_SETTINGS);
    $form['server_uri'] = [
      '#type' => 'url',
      '#default_value' => $config->get('server_uri'),
      '#title' => $this->t('Crowd server URI'),
      '#required' => TRUE,
      '#description' => $this->t('URI of Crowd server.'),
    ];
    $form['username'] = [
      '#type' => 'textfield',
      '#default_value' => $config->get('username'),
      '#title' => $this->t('Username'),
      '#required' => TRUE,
      '#description' => $this->t('Username to authenticate against Crowd server with.'),
    ];
    $form['password'] = [
      '#type' => 'item',
      '#title' => $this->t('Password'),
      '#markup' => $this->t('Provided by the Key module - <a href=":url">Edit</a>', [
        ':url' => Url::fromUri('internal:/admin/config/system/keys/manage/crowd_password')->toString(),
      ]),
    ];
    $form['email_as_username'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('email_as_username'),
      '#title' => $this->t('Use email for username'),
      '#description' => $this->t('When registering users, use email only instead of a username. Please note this will prevent users from being able to change their email address, as Crowd does not allow changing usernames.'),
    ];
    $form['restricted_domains'] = [
      '#type' => 'textarea',
      '#default_value' => implode("\n", $config->get('restricted_domains')),
      '#title' => $this->t('Restricted domains'),
      '#required' => FALSE,
      '#description' => $this->t('List of restricted domains, one per line. Users with email addresses from these domains will not be able to update their password or delete their account.'),
    ];
    $role_storage = $this->entityTypeManager->getStorage('user_role');
    $role_ids = $role_storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('id', [
        RoleInterface::AUTHENTICATED_ID,
        RoleInterface::ANONYMOUS_ID,
      ], 'NOT IN')
      ->execute();
    $form['verified_role'] = [
      '#type' => 'select',
      '#options' => array_map(fn (RoleInterface $role) => $role->label(), $role_storage->loadMultiple($role_ids)),
      '#title' => $this->t('Verified role'),
      '#description' => $this->t('Optional role to grant users who have verified their email address.'),
      '#default_value' => $config->get('verified_role'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    $config = $this->configFactory()->getEditable(self::CROWD_SETTINGS);
    $config->set('username', $form_state->getValue('username'))
      ->set('server_uri', $form_state->getValue('server_uri'))
      ->set('email_as_username', $form_state->getValue('email_as_username'))
      ->set('verified_role', $form_state->getValue('verified_role'))
      ->set('restricted_domains', array_map('trim', explode("\n", $form_state->getValue('restricted_domains'))))
      ->save();
  }

}
